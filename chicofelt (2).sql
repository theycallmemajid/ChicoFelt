-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2017 at 08:35 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chicofelt`
--

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `quantity` tinyint(3) UNSIGNED NOT NULL,
  `address` text NOT NULL,
  `ordered_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `order_status` enum('submited','sended') NOT NULL DEFAULT 'submited',
  `sent_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `product_id`, `quantity`, `address`, `ordered_ts`, `order_status`, `sent_ts`) VALUES
(1, 1, 56, 2, 'تهران', '2017-11-17 18:08:33', 'sended', '2017-11-21 11:30:29'),
(2, 37, 57, 1, 'شهریار', '2017-11-20 08:15:31', 'sended', '2017-11-21 11:30:29'),
(4, 1, 56, 5, 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف استفاده قرار گیرد.', '2017-11-21 06:20:53', 'sended', '2017-11-21 11:34:32'),
(5, 38, 1, 1, 'تهران خیابان الف نرسیده به ب کوچه پ پلاک 66', '2017-11-21 11:31:32', 'sended', '2017-11-21 11:33:52'),
(6, 39, 57, 2, 'ایران تهران', '2017-11-21 11:55:00', 'submited', '2017-11-21 11:55:00'),
(7, 40, 62, 1, 'تهران میدان حر', '2017-11-28 14:04:25', 'submited', '2017-11-28 14:04:25'),
(8, 40, 57, 2, 'تهران میدان حر', '2017-11-28 14:04:26', 'submited', '2017-11-28 14:04:26'),
(9, 40, 56, 1, 'تهران میدان حر', '2017-11-28 14:04:26', 'submited', '2017-11-28 14:04:26'),
(10, 40, 1, 1, 'کرج میدان نبوت', '2017-11-28 14:10:20', 'submited', '2017-11-28 14:10:20');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(30) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `location_img` varchar(255) DEFAULT NULL,
  `price` mediumint(8) UNSIGNED NOT NULL,
  `created_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `location_img`, `price`, `created_ts`) VALUES
(1, 'نمد خرگوشی', 'عالیه این نمده بخر! نمدای ترد و خوشمزه 4 تومن همکارا فقط هزار', 'Wallpaper (45).jpg', 5000, '2017-11-15 12:37:37'),
(56, 'نمد سیاه', 'نمدی خرگوشی یکی از پرفروش ترین محصولات سایت چی کلفت است', 'Wallpaper (70).jpg', 20000, '2017-11-17 13:39:22'),
(57, 'نمد الکی', 'یک نمد بسیار زیبا برای شما!', 'Wallpaper (40).jpg', 17000, '2017-11-17 17:36:04'),
(62, 'djkok kjjbeue', 'nxcjbdcbdcu scue heunxcjbdcbdcu scue heunxcjbdcbdcu scue heu', 'Wallpaper (8).jpg', 5600, '2017-11-21 14:03:48');

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE `rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `rate_number` tinyint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rates`
--

INSERT INTO `rates` (`id`, `product_id`, `user_id`, `rate_number`) VALUES
(1, 1, 1, 3),
(5, 57, 37, 5),
(6, 62, 40, 3),
(7, 56, 40, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reset_pass`
--

CREATE TABLE `reset_pass` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `hash` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(15) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `lname` varchar(20) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `email` varchar(30) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `role` enum('admin','user') NOT NULL DEFAULT 'user',
  `pass` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `email`, `mobile`, `role`, `pass`) VALUES
(1, 'محمد', 'قارلقی', 'mohammad7th@gmail.com', '09381281569', 'admin', '93ee0cc2571ef95a983536281b8d4006'),
(37, 'مجید', 'شهبانی', 'theycallmemajid@gmail.com', '09211275578', 'admin', '93ee0cc2571ef95a983536281b8d4006'),
(38, 'بیتا', 'مشرقی', 'bita@mail.ir', '09381278989', 'user', '93ee0cc2571ef95a983536281b8d4006'),
(39, 'سامان', 'ایران نژاد', 'saman@gmail.com', '09123456789', 'user', 'd217148e393b423554d80e937c9ee290'),
(40, 'مجید', 'سلمانی', 'majid3th@gmail.com', '09121275578', 'user', '93ee0cc2571ef95a983536281b8d4006');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `reset_pass`
--
ALTER TABLE `reset_pass`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `rates`
--
ALTER TABLE `rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `reset_pass`
--
ALTER TABLE `reset_pass`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `carts_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `carts_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `rates`
--
ALTER TABLE `rates`
  ADD CONSTRAINT `rates_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `rates_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `reset_pass`
--
ALTER TABLE `reset_pass`
  ADD CONSTRAINT `reset_pass_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
