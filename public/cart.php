<?php require_once "../resources/functions.php" ?>
<?php
if (count($_SESSION['products']) < 1) {
    setcookie("carting", null, time() - (60 * 60 * 24 * 365), "/");
}
if (isset($_GET['deleteitem'])) {
    if (isset($_SESSION['products'][$_GET['deleteitem']])) {
        unset($_SESSION['products'][$_GET['deleteitem']]);
        if (count($_SESSION['products']) === 0) {
            setcookie("carting", null, time() - (60 * 60 * 24 * 365), "/");
            redirectTo(CART_URL);


        }
    }
}

if (isset($_GET['id'])) {
    if (!isset($_COOKIE['carting'])) {
        setcookie("carting", true, time() + (60 * 60 * 24 * 365), "/");
        $_SESSION['products'][$_GET['id']] = 1;
        redirectTo(CART_URL);

    } else {
        $_SESSION['products'][$_GET['id']] = 1;
        redirectTo(CART_URL);


    }

}
?>
<!doctype html>
<html lang="fa-IR">
<meta charset=" UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">

<title>چیکوفلت</title>
</head>
<?php require_once TMP_FRONT . DS . "mainHeader.php" ?>
<div class="row w-100 h-100 pt-5 text-center chicoWhite m-0 mt-5 text-center">
    <?php if (isset($_COOKIE['carting'])): ?>

    <form action="payment.php" method="post" class="w-100">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>عنوان</th>
                <th dir="rtl">قیمت (تومان)</th>
                <th>تعداد</th>
                <th dir="rtl">جمع (تومان)</th>
                <th dir="rtl">حذف</th>

            </tr>
            </thead>
            <tbody>
            <?php $sum = 0 ?>
            <?php
            foreach ($_SESSION['products'] as $key => $val):?>
                <?php $product = getProduct($key);
                ?>
                <tr>
                    <input type="hidden" name="products[]" value="<?php echo $product['id'] ?>">
                    <td><?php echo $product['id'] ?></td>
                    <td><?php echo $product['title'] ?></td>
                    <td><?php echo $product['price'] ?></td>
                    <td>
                        <select name="number<?php echo $product['id'] ?>" id="s<?php echo $product['id'] ?>"
                                data-pid="<?php echo $product['id'] ?>" class="number custom-select">
                            <option value="1" selected>1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </td>
                    <td class="sumRow" id="p<?php echo $product['id'] ?>"
                        data-price="<?php echo $product['price'] ?>"><?php echo $product['price'] ?></td>
                    <td><a href="?deleteitem=<?php echo $product['id'] ?>"><span class="fa fa-times-circle text-danger"
                                                                                 style="cursor: pointer"></span></a>
                    </td>
                </tr>
                <?php $sum += $product['price']; ?>
            <?php endforeach; ?>
            </tbody>

        </table>


        <?php endif; ?>
        <?php if (!isset($_GET['id'])): ?>
            <?php if (!isset($_COOKIE['carting'])): ?>
                <div class="container">
                    <h1 class="text-center text-danger font-weight-bold" dir="rtl"><span
                                class="fa fa-shopping-cart"></span>
                        سبد
                        خرید شما خالی است :(
                    </h1>
                </div>
            <?php endif; ?>
        <?php endif; ?>

</div>
<?php if (isset($_SESSION['products'])): ?>
    <div class="alert alert-success text-center">
        <div class="container">
            <p class="h2 font-weight-bold" dir="rtl">مبلغ کل :</p>
            <p class="mb-0" id="finalSum"><?php echo $sum; ?></p>
            <p>تومان</p>
        </div>
    </div>
    <div class="form-group mt-5">
        <textarea name="address" id="address" cols="30" rows="10" class="form-control mx-auto w-50 text-right" dir="rtl"
                  required placeholder="لطفا آدرس خود را وارد نمایید!"></textarea>
    </div>
<?php endif ?>
<?php if (isLoggedIn() and  isset($_SESSION['products'])): ?>
    <div class="w-50 mx-auto text-center " style="margin-bottom: 6rem">
        <button class="btn text-white btn-block btn-success" style="cursor: pointer" type="submit" name="sub">ثبت
            سفارش
        </button>
    </div>
<?php endif; ?>
<?php if (!isLoggedIn()): ?>
    <div class="w-50 mx-auto text-center">
        <a href="auth.php" class="text-dark" style="cursor: pointer" dir="rtl">برای ثبت سفارش باید عضو شوید!</a>
    </div>
<?php endif; ?>

</form>
<?php include_once TMP_FRONT . DS . "bottomBanner.php" ?>


<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/app.js"></script>
</body>
</html>

