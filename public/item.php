<?php require_once "../resources/functions.php" ?>
<?php if (isset($_GET['id'])): ?>
    <!doctype html>

    <html lang="fa-IR">
    <meta charset=" UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/jquery.rateyo.min.css">
    <link rel="stylesheet" href="css/style.css">

    <title>چیکوفلت</title>
    <body>
    </head>
    <?php require_once TMP_FRONT . DS . "mainHeader.php" ?>
    <div class="row w-100 h-100 pt-5 chicoWhite m-0 mt-4 text-center">
        <?php $product = getProduct($_GET['id']); ?>
        <div class="col-sm-6">
            <img src="../resources/uploads/<?php echo $product['location_img'] ?>" alt="" class="w-100 rounded ">
        </div>
        <?php
        $d = new DateTime($product['created_ts']);
        $date = jdate("j F o  G:i", $d->getTimestamp());
        ?>
        <div class="card col-sm-6 p-0">
            <h4 class="card-header card-title m-0 font-weight-bold"><?php echo $product['title'] ?></h4>

            <div class="card-block p-3">
                <div class="card-text py-3" dir="rtl"><?php echo $product['description'] ?></div>
                <?php if(isOrdered(currentUser(),$product['id'])): ?>
                    <p class="text-muted">خریده شده</p>
                <?php endif; ?>
                <hr>
                <div class="card-text float-left">
                    <div  data-rateyo-read-only="<?php
                    if (isLoggedIn()) {
                        if (isOrdered($_SESSION['user_id'], $product['id']) and ratedBefore($_SESSION['user_id'], $product['id'])) {
                            echo 'true';
                        } else if (isOrdered($_SESSION['user_id'], $product['id'])) {
                            echo 'false';
                        }else{
                            echo 'true';
                        }
                    } else {
                        echo 'true';
                    }
                    ?>" class="float-left rateYo" data-rateyo-full-star="true"
                         data-rateyo-rating="<?php echo ceil(getRate($product['id'])['ave']) ?>"></div>
                    <span class=" text-warning" id="ratenumber"
                          style="font-size: .8rem;">(<?php echo getRate($product['id'])['cnt'] ?>)</span>
                    <?php if(isRated(currentUser(),$product['id'])): ?>
                        <span class="text-muted font-weight-bold" style="font-size: .8rem">قبلا رای داده شده</span>
                    <?php endif; ?>
                </div>
                <p class="card-text float-right text-muted " dir="rtl"><span
                            class="text-success"><?php echo number_format($product["price"] )?></span>تومان</p>
                <div class="clearfix"></div>
                <div class="card-text">
                    <a href="cart.php?id=<?php echo $product['id'] ?>" class="btn btn-success" id="addToCart">اضافه به
                        سبد خرید</a>
                </div>

            </div>
            <div class="card-footer text-muted m-0" dir="rtl"><?php echo $date ?></div>
            <span id="info" data-product="<?php echo $product['id'] ?>" data-user="<?php if (isLoggedIn()) {
                echo $_SESSION['user_id'];
            } else {
                echo 0;
            } ?>"></span>

        </div>


    </div>
    <br>
    <div class="alert-danger alert text-center h4" style="display: none;" dir="rtl" id="notloggedin">شما برای رای دادن
        باید وارد سایت شوید!
    </div>
    <div class="alert-success alert text-center h4" style="display: none;" dir="rtl" id="successfullRate">رای شما با
        موفقیت ثبت شد!
    </div>
    <div class="alert-danger alert text-center h4" style="display: none;" dir="rtl" id="notordered">شما برای نظر دادن
        باید حتما محصول را خریده باشید!
    </div>

    <?php include_once TMP_FRONT . DS . "footer.php" ?>
    <?php include_once TMP_FRONT . DS . "bottomBanner.php" ?>


    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.rateyo.min.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    </body>
    </html>
<?php endif; ?>
<?php if (!isset($_GET['id'])) {
    redirectTo(HOME_URL);
} ?>