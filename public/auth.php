<?php require_once "../resources/functions.php" ?>
<?php if (isLoggedIn()){
    redirectTo(HOME_URL);
} ?>

<!doctype html>

<html lang="fa-IR">
<meta charset=" UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css">

<title>چیکوفلت</title>
</head>
<body>


<?php include_once TMP_FRONT . DS . "altMainHeader.php" ?>

<?php include_once TMP_FRONT . DS . "authentication.php" ?>



<?php include_once TMP_FRONT . DS . "bottomBanner.php" ?>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/vue.js"></script>
<script type="text/javascript" src="js/jquery.rateyo.min.js"></script>

<script type="text/javascript" src="js/app.js"></script>
</body>
</html>