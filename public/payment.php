<?php
include_once "../resources/functions.php";
// if it submitted!
if (isset($_POST['sub'])) {
    $_SESSION['cart_status'] = "block";
    $address = $_POST['address'];
    $products = $_POST['products'];
    $boughtOnes = [];
    $sum = 0;
    foreach ($products as $product) {
        $sum += getProduct($product)['price'] * $_POST['number' . $product];
        $boughtOnes[] = [$product => $_POST['number' . $product]];
    }

    foreach ($boughtOnes as $bought){
    foreach ($bought as $pid=>$quantity){
        addOrder(currentUser(),$pid,$quantity,$address);
    }
    }


}
?>
<!doctype html>

<html lang="fa-IR" class="h-100">
<meta charset=" UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/jquery.rateyo.min.css">
<link rel="stylesheet" href="css/style.css">

<title> صفحه پرداخت بانکی</title>
</head>
<body class="h-100 text-right">
<div class="row m-0">
    <div class="justify-content-center col-sm-6 text-right mx-auto">
        <form action="payback.php" method="post">
            <h2 dir="auto">صفحه پرداخت بانکی :</h2>
            <div class="form-group">
                <label class="d-block">
                    وضعیت
                    <input type="text" name="status" value="100" class="form-control">
                </label>
            </div>
            <div class="form-group">
                <label class="d-block">
                    کد پیگیری
                    <input type="text" name="refnumber" value="<?php echo getHash(rand(100, PHP_INT_MAX)); ?>"
                           class="form-control">
                </label>
            </div>
            <button class="btn-block btn btn-success" name="paymentsubmit">ثبت</button>
        </form>
    </div>
</div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.rateyo.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/vue.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="js/admin.js"></script>
</body>
</html>

