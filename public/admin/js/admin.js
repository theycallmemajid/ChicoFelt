var app = new Vue({
    el: "#app",
    data: function () {
        return {

            showNewNamad: false,
        }
    },
    methods: {
        addProduct: function () {
            var sw = false;
            $.ajax({
                url: "http://localhost:81/chicofelt/resources/addProduct.php",
                method: "post",
                data: {
                    title: $("#title").val(),
                    price: $("#price").val(),
                    description: $("#desc").val(),
                    submit: true

                },
                success: function (response) {
                    if (response) {
                        sw = true
                    }
                }
            });
            // this.showNewNamad=sw;
            return sw;
        },

    }
});
$(document).ready(function () {
    // $(".xButton").click(function (e) {
    //
    //     var userId = $(this).data("userid");
    //
    //
    //     $("#deleteUser").click(function (ev) {
    //
    //         $.ajax({
    //             url: "http://localhost:81/chicofelt/resources/deleteUser.php",
    //             method: "GET",
    //             data: {
    //                 userfordelete: userId,
    //                 submit: true,
    //             },
    //             success: function (response) {
    //                 var st = "#" + userId;
    //                 $(st).fadeOut();
    //                 $("#myalert").fadeIn();
    //                 setTimeout(function () {
    //                     $("#myalert").fadeOut();
    //                 }, 2000)
    //             }
    //
    //         });
    //     });
    //
    //
    // });

    $(".editButton").click(function () {
        $("#editForm").slideDown();
        $("#hidden").val($(this).data("userid"));

    });
    $(".sendorder").click(function () {
        var orderId = $(this).data("orderid");
        $.ajax({
            url: "http://localhost:81/chicofelt/resources/sendOrder.php",
            method: "POST",
            data:{
                orderid:orderId
            },
            success:function (response) {
                console.log(response);
                location.reload();
            }
        });

    });
    $(".editproduct").click(function () {
        var productId = $(this).data("productid");
        $("#hidden").val(productId);
        $.ajax({
            url:"http://localhost:81/chicofelt/resources/getProduct.php",
            method:"GET",
            data:{
                product_id:productId
            },
            success:function (response) {
                $("#title").val(response.title);
                $("#price").val(response.price);
                $("#description").val(response.description)
                $("#editProduct").fadeIn();
        }});



    });


});
//ready
