
$(document).ready(function () {

    $(document).scroll(function () {
        if ($(document).scrollTop() >= 10) {
            $(".chicoNavbar .container a.chicoLogo").css({"font-size": "1.8rem"});

        } else {
            $(".chicoNavbar .container a.chicoLogo").css({"font-size": "1.5rem"});

        }
    });

    $('#search').keyup(function () {

        if($(this).val().length<=4){
            $("#searchForm div").css("display","none");

        }
        if($(this).val().length>=3){
        $.ajax({
            url: 'http://localhost:81/chicofelt/resources/search.php',
            method:'post',
            data:{
                search:$('#search').val()
            },
            success:function (response) {
                var h = "";
                $("#searchForm div").css("display","block");
                for(var i=0;i<=response.length;i++){
                    h += "<div class='text-right'><h5 class='pt-2'>"+"<a href='' class='text-white ' style='text-decoration: none'>"+response[i].title+"</a>"+"</h5>" +
                        "<a href='' class='text-dark' style='font-size: .8rem;text-decoration: none'>"+response[i].description+"</a>" +
                        " <a href='' class='text-left text-danger font-weight-bold' style='font-size: .9rem;text-decoration: none'>نمایش جزئیات</a>" +
                        "</div>";
                    $("#searchForm div").html(h);
                }




            }
        });
        }
    });

    $("input#loginSubmit").click(function (e) {
        if (!validateLogin()) {
            e.preventDefault();
            $("form#log p").addClass("alert").addClass("alert-warning").text("ایمیل یا رمزعبور را وارد نمایید!");
        }
        e.submit;
    });
    $("input#registerSubmit").click(function (e) {
        if (!validateRegister()) {
            e.preventDefault();
            $("form#reg p").addClass("alert").addClass("alert-warning").text("لطفا تمام فرم را پر کنید !");
        }
        e.submit;
    });
    $(".rateYo").rateYo({
    starWidth:'18px'
    });


});
function validateLogin() {
    if (($("input#loginEmail").val().length === 0) || ($("input#loginPass").val().length === 0)) {
        return false;
    } else {
        var reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (reg.test($("input#loginEmail").val())) {
            return true;
        }
        else {
            return true;
        }
    }
}
function validateRegister() {
    if (($("input#email").val().length === 0) || ($("input#passOne").val().length === 0) || ($("input#passTwo").val().length === 0) || ($("input#fname").val().length === 0)) {
        return false;
    } else {
        var reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var num = /09([123])\d{8}/;
        if (reg.test($("input#email").val()) && num.test($("input#mobile").val())) {
            return true;
        }
        else {
            return true;
        }
    }
}


