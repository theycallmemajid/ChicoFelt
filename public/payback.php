<?php
include_once "../resources/functions.php";
if (isset($_POST['paymentsubmit']) and isset($_POST['status']) and $_POST['status'] == 100) {
    $refnum = $_POST['refnumber'];
    $status=true;
    unset($_SESSION['products']);
    $_SESSION['cart_status'] = "free";
}else {
    $status = false;
}
?>

<!doctype html>

<html lang="fa-IR" class="h-100">
<meta charset=" UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/jquery.rateyo.min.css">
<link rel="stylesheet" href="css/style.css">

<title> صفحه پرداخت بانکی</title>
</head>
<body class="h-100 text-right">
<?php if($status){ ?>
<div class="alert alert-success text-center" dir="rtl">
    <p class="h3">خرید شما با موفقیت انجام شد!</p>
    <p class="h5">
        <span>شماره پیگیری شما :</span>
        <span><?php echo $refnum ?></span>
    </p>
    <div>
        <a href="<?php echo HOME_URL ?>" CLASS="btn btn-success">ورود به صفحه اصلی</a>
    </div>
</div>
<?php }else{?>
<div class="alert alert-danger text-center" dir="rtl">
    <p class="h3">خرید شما با موفقیت انجام نشد!</p>

    <div>
        <a href="<?php echo HOME_URL ?>" CLASS="btn btn-danger">ورود به صفحه اصلی</a>
    </div>
</div>
<?php } ?>


<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.rateyo.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/vue.js"></script>
<script type="text/javascript" src="js/app.js"></script>
<script type="text/javascript" src="js/admin.js"></script>
</body>
</html>
