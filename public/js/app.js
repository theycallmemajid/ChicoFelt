function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
$(document).ready(function () {

    $(".number").change(function (e) {
        var pid = $(this).data("pid");
        var price = $("#p"+pid).data("price");
        $("#p"+pid).text(price*$(this).val());
        $("#finalSum").text(function () {
            var sum=0;
            $(".sumRow").each(function () {
               sum+=parseInt($(this).text());
            });
            return sum;
        });

    });

    var thisPage = getUrlVars()['page'];
    if (thisPage === undefined) {
        thisPage = 1;
    }
    var pageId = "#p" + thisPage;

    $(pageId).parent().addClass("active");
    $(".page-link").click(function () {
        if ($(this).parent().hasClass("active")) {

        } else {
            $(".page-link").parent().removeClass("active");
            $(this).parent().addClass("active");

        }
    });


    $(document).scroll(function () {
        if ($(document).scrollTop() >= 10) {
            $(".chicoNavbar .container a.chicoLogo").css({"font-size": "1.8rem"});

        } else {
            $(".chicoNavbar .container a.chicoLogo").css({"font-size": "1.5rem"});

        }
    });

    $('#search').keyup(function () {

        if ($(this).val().length <= 4) {
            $("#searchForm div").css("display", "none");

        }
        if ($(this).val().length >= 3) {
            $.ajax({
                url: 'http://localhost:81/chicofelt/resources/search.php',
                method: 'post',
                data: {
                    search: $('#search').val()
                },
                success: function (response) {
                    console.log(response);
                    var h = "";
                    $("#searchForm div").css("display", "block");
                    for (var i = 0; i <= response.length; i++) {
                        h += "<div class='text-right'><h5 class='pt-2'>" + "<a href='item.php?id=" + response[i].id + "' class='text-white ' style='text-decoration: none'>" + response[i].title + "</a>" + "</h5>" +
                            "<a href='' class='text-dark' style='font-size: .8rem;text-decoration: none'>" + response[i].description + "</a>" +
                            " <a href='item.php?id=" + response[i].id + "' class='text-left text-danger font-weight-bold' style='font-size: .9rem;text-decoration: none'>نمایش جزئیات</a>" +
                            "</div>";
                        $("#searchForm div").html(h);
                    }


                }
            });
        }
    });

    $("input#loginSubmit").click(function (e) {
        if (!validateLogin()) {
            e.preventDefault();
            $("form#log p").addClass("alert").addClass("alert-warning").text("ایمیل یا رمزعبور را وارد نمایید!");
        }
        e.submit;
    });
    $("input#registerSubmit").click(function (e) {
        if (!validateRegister()) {
            e.preventDefault();
            $("form#reg p").addClass("alert").addClass("alert-warning").text("لطفا تمام فرم را پر کنید !");
        }
        e.submit;
    });
    $(".rateYo").rateYo({
        starWidth: '18px',
        numStars: 5,
        maxValue: 5,
        onSet: function (rating, rateYoInstance) {
            var userid = $("#info").data("user");
            var productid = $("#info").data("product");

            if (userid !== 0) {
                $.ajax({
                    url: 'http://localhost:81/chicofelt/resources/rating.php',
                    method: "POST",
                    data: {
                        islogged: true,
                        user: userid,
                        product: productid,
                        ratenum: rating
                    },
                    success: function (response) {
                        console.log(response);
                        if (response == 1) {
                            $("#successfullRate").show();
                        } else if (response == 2) {
                            $("#notordered").fadeIn("slow", "swing");
                        } else if (response == 3) {
                            $("#notloggedin").fadeIn("slow", "swing");

                        }
                    },
                });
            } else {
                $("#notloggedin").fadeIn("slow", "swing");

            }

        }
    });


});
function validateLogin() {
    if (($("input#loginEmail").val().length === 0) || ($("input#loginPass").val().length === 0)) {
        return false;
    } else {
        var reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (reg.test($("input#loginEmail").val())) {
            return true;
        }
        else {
            return true;
        }
    }
}
function validateRegister() {
    if (($("input#email").val().length === 0) || ($("input#passOne").val().length === 0) || ($("input#passTwo").val().length === 0) || ($("input#fname").val().length === 0)) {
        return false;
    } else {
        var reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var num = /09([123])\d{8}/;
        if (reg.test($("input#email").val()) && num.test($("input#mobile").val())) {
            return true;
        }
        else {
            return true;
        }
    }
}


