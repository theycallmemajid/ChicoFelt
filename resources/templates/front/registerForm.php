<div class="container">
    <h4 class="text-center pt-5 text-primary">عضویت</h4>
    <p class="text-muted text-center" dir="rtl" style="font-size: .85rem">از اینکه داری به جمع ما می پیوندی
        خوشحالیم.</p>
    <form id="reg" class="mt-5 text-center " dir="rtl" method="post">
        <div class="container">
            <p></p>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        <input type="text" class="form-control" id="fname" name="fname"
                               placeholder="نام مثلا: علی">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                        <input type="text" class="form-control  " id="lname" name="lname"
                               placeholder="نام خانوادگی مثلا: ایرانی پور">
                    </div>
                </div>


            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        <input type="email" class="form-control  " id="email" name="email" required
                               placeholder="ایمیل مثلا: sinairanpour@gmail.com ">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                        <input type="text" class="form-control" id="mobile" name="mobile" required
                               placeholder="شماره موبایل مثلا: 09121200120">
                    </div>
                </div>


            </div>
            <div class="form-group ">
                <input type="password" class="form-control" id="passOne" name="passOne" required
                       placeholder="رمز عبور">
            </div>
            <div class="form-group ">
                <input type="password" class="form-control" id="passTwo" name="passTwo" required
                       placeholder="تکرار رمز عبور">
            </div>
            <div class="form-group">
                <input type="submit" value="عضویت" id="registerSubmit" name="registerSubmit"
                       class="btn btn-primary btn-block">
            </div>


        </div>

        <?php
        $re = true;
        if (isset($_POST['registerSubmit'])) {
            $re = addUser($_POST['fname'], $_POST['lname'], $_POST['email'], $_POST['mobile'], $_POST['passOne'], $_POST['passTwo']);
        }
        ?>
        <?php if (!$re): ?>
            <p class="alert-danger alert text-center mt-4" dir="rtl">خطایی رخ داده است !</p>
        <?php endif; ?>
    </form>

</div>

