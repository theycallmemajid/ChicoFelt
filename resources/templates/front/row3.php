<!--Row3-->
<div class="row3 text-center bg-warning chicoWhite w-100 " style="height: 20rem;box-sizing: border-box;padding: 0 ;">
    <h2 class="pt-5 font-weight-bold">عضویت در خبرنامه <span class="text-danger">فودتو</span></h2>
    <p>برای عضویت در خبرنامه ما کافیه فقط ایمیلتو این زیر بنویسی</p>
    <div class="container ">
        <form action="">

            <div class="input-group mb-2 mx-auto mb-sm-0" id="newLetterContainer">
                <button class="input-group-addon" type="submit" id="inputAddOn"><span class="fa fa-reply"></span>
                </button>
                <input type="email" class="text-center w-100 " required id="newLetterInput" placeholder="ایمیل">
            </div>
            <p class="text-center mt-5 " style="font-weight: 300">برای عضویت در خبرنامه فودتو و دریافت جدیدترین
                مطالب تخفیف‌‌ها و جشنواره‌ها ایمیل خود را وارد کنید</p>

            <!--end Input container-->
        </form>
    </div>
    <!--end container-->

</div>
<!--End row3-->