<div class="container">
    <h4 class="text-center pt-5 text-warning">ورود</h4>
    <p class="text-muted text-center" dir="rtl" style="font-size: .85rem">ایمیل خود را وارد کنید.</p>
    <form id="log" class="mt-5 text-center " dir="rtl" method="post">
        <div class="container">

            <div class="form-group ">
                <p></p>


                <input type="email" class="form-control" id="loginEmail" name="loginEmail"
                       placeholder="ایمیل">
            </div>
            <div class="form-group ">
                <input type="password" class="form-control" id="loginPass" name="loginPass"
                       placeholder="رمز عبور">
            </div>
            <div class="form-group">
                <input type="submit" value="ورود" id="loginSubmit" name="loginSubmit"
                       class="btn btn-warning text-white btn-block">
            </div>
            <a href="" dir="rtl">
                <small>رمز عبورتو فراموش کردی؟</small>
            </a>


        </div>


    </form>
    <?php
    $lo = true;
    if (isset($_POST['loginSubmit'])) {
        $lo = loggIn($_POST['loginEmail'], $_POST['loginPass']);
    }
    ?>
    <?php if (!$lo): ?>
        <p class="alert-danger alert text-center mt-4" dir="rtl">ایمیل یا رمز عبور اشتباه است .</p>
    <?php endif; ?>

</div>
