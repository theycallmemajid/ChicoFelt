<!--Row1-->
<div class="row1 h100 ">
    <?php include_once "mainHeader.php" ?>
    <!--Slider-->
    <div class="chicoSlider h-100 w-100" style="background-image: url('img/namad.jpg');">
        <div class="container text-center">
            <h3 class="text-white text-center font-weight-bold chicoText" style="padding-top: 10rem;direction: rtl">چیکوفلت نمدی
                های زیبا</h3>
            <p class="text-white text-center pt-2 chicoText" style="direction: rtl">بهترین کیفیت نمد در ایران</p>
            <div class="row pt-4">
                <div class="col-sm-8 offset-sm-2">
                    <form id="searchForm">
                        <p class="chicoSearchContainer">
                            <input type="text" class="w-100 " placeholder="بگرد: مثلا خرگوش..." id="search"
                                   name="search"
                                   style="direction: rtl;">
                            <span class="fa fa-search"></span>
                        <div class="w-100 bg-trans text-white p-3" style="display: none">

                        </div>
                        </p>
                        <!--end Input container-->
                    </form>
                    <!--searchbox form-->
                </div>
                <!--end col-->
            </div>
            <!--End row-->
        </div>
        <!--container-->

    </div>
    <!--End Slider-->
</div>
<!--End Row1-->