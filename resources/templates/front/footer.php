<footer id="myFooter" class="text-center mt-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-3 col-md-12">
                <h2 class="logo"><a href="#" style="font-family: Samim"> چیـکوفـلـت </a></h2>
            </div>
            <div class="col-sm-2">
                <h5 class="text-danger"  style="border-top: .4rem solid mediumvioletred;padding-top: 1rem">شروع کن</h5>
                <ul>
                    <li><a href="#">خانه</a></li>
                    <li><a href="#">عضویت</a></li>
                    <li><a href="#">خریدها</a></li>
                </ul>
            </div>
            <div class="col-sm-2">
                <h5 class="text-danger" style="border-top: .4rem solid cyan;padding-top: 1rem">درباره ما</h5>
                <ul>
                    <li><a href="#">اطلاعات ما</a></li>
                    <li><a href="#">ارتباط با ما</a></li>
                    <li><a href="#">نظرات</a></li>
                </ul>
            </div>
            <div class="col-sm-2">
                <h5 class="text-danger" style="border-top: .4rem solid mediumpurple;padding-top: 1rem">پشتیبانی</h5>
                <ul>
                    <li><a href="#">سوالات متداول</a></li>
                    <li><a href="#">سوال داری باز؟</a></li>
                    <li><a href="#">انجمن</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3">
                <div class="social-networks">
                    <a href="#" class="twitter"><i class="fa fa-telegram"></i></a>
                    <a href="#" class="facebook"><i class="fa fa-instagram"></i></a>
                    <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
                </div>
                <button type="button" class="btn btn-default" dir="rtl" style="cursor: pointer">باهامون حرف بزن!</button>
            </div>
        </div>
    </div>
    <div class="footer-copyright mb-5">
        <p>© 2017 Chicofelt All rights reserved. </p>
    </div>
</footer>