<!--Row2-->
<div class="row2 h-100 chicoWhite  w-100">
    <!--Row2 Header-->
    <div class="orderContainer text-center pt-5 ">
        <h2 style="">نـمـدهـای خوشـکـل</h2>
        <p>از هر رنگ و نقش</p>
    </div>
    <!--End Row2 Header-->


    <?php include_once "cards.php" ?>
    <!-- /.Cards   -->
    <div class="row m-0 text-center ">
        <span class="fa fa-refresh fa-spin fa-5x text-center justify-content-center mx-auto" id="spinner"
              style="display: none"></span>
    </div>

    <nav class="text-center my-5">
        <div class="container">
            <ul class="pagination justify-content-center ">
                <li class="page-item disabled">
                    <a class="page-link" href="" tabindex="-1">قبلی</a>
                </li>
                <?php
                if (productsCount()['cnt'] === 0) {
                    $pages = floor(productsCount()['cnt'] / MAXITEM);
                } else {
                    $pages = floor(productsCount()['cnt'] / MAXITEM) + 1;
                }
                ?>
                <?php for ($i = 1; $i <= $pages; $i++): ?>
                    <li class="page-item"><a class="page-link" id="p<?php echo $i ?>"
                                              href="?page=<?php echo $i ?>"><?php echo $i ?></a></li>
                <?php endfor; ?>

            </ul>
        </div>
    </nav>
    <!--/.Pagination-->


</div>
<!--End row2-->
