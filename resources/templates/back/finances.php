<div class="col-sm-10 chicoTextBlue pt-5 h-100" id="orders">
    <h3 class=" h1 text-center chicoTextBlue">
        <span class="font-weight-bold">اطلاعات</span>
        <span class="fa fa-info-circle"></span>
    </h3>
    <hr>
    <br>
    <div class="row m-0">
    <div class="card chicoBlue col-sm-3 text-white text-center m-3" style="height: 15rem;">
        <div class="card-block pt-3">
            <h2 class="font-weight-bold pt-4" style=" font-family: Shabnam;" dir="rtl"  >
                <span>میزان فروش کل : </span>
            </h2>
            <h4 class="font-weight-bold pt-5 h1" style=" font-family: Shabnam;" dir="rtl" >
                <span class="font-weight-bold text-warning"><?php echo number_format(getOrdersSumSale()['jam'])?></span>
                <span>تومان</span>
            </h4>
            <br><br>
        </div>
    </div>
    <!--card -->

    <div class="card chicoBlue col-sm-3 text-white text-center m-3" style="height: 15rem;">
        <div class="card-block pt-3">
            <h2 class="font-weight-bold pt-4" style=" font-family: Shabnam;" dir="rtl"  >
                <span>تعداد فروش کل : </span>
            </h2>
            <h4 class="font-weight-bold pt-5 h1" style=" font-family: Shabnam;" dir="rtl" >
                <span class="font-weight-bold text-warning"><?php echo getOrdersNumberOfSale()['cnt'] ?></span>
                <span>عدد</span>
            </h4>
            <br><br>
        </div>
    </div>
    </div>


</div>

