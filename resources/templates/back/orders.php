<div class="col-sm-10 chicoTextBlue pt-5 h-100" id="orders">
<h3 class="text-right">سفارشات</h3>
    <br>
    <table class="table text-justify text-center table-hover" dir="rtl">
        <tr>
            <th>#</th>
            <th>نام و نام خانوادگی</th>
<!--            <th>قیمت کل</th>-->
            <th>تعداد</th>
            <th>نام نمد</th>
            <th>آدرس</th>
            <th>تاریخ سفارش</th>
            <th>تایید سفارش</th>
        </tr>

        <?php foreach (getOrders() as $key => $value): ?>
            <?php
            $d = new DateTime($value['ordered_ts']);
            $date = jdate("j F o  G:i", $d->getTimestamp());
            $jdate = new jDateTimePlus(true,true,"Asia/Tehran");
            ?>
            <tr>
                <?php $user = getUser($value['user_id']); ?>
                <td><?php echo $value['id'] ?></td>
                <td><?php echo $user['fname']." ".$user['lname'] ?></td>
<!--                <td></td>-->
                <td><?php echo $value['quantity'] ?></td>
                <td><?php echo getProduct($value['product_id'])['title'] ?></td>
                <td class="text-justify" ><?php echo $value['address'] ?></td>

                <td><?php echo $jdate->date("l j F Y H:i",$d->getTimestamp()); ?></td>
                <td ><span class="fa fa-check text-success sendorder" style="cursor: pointer" data-orderid="<?php echo $value['id']?>"></span></td>

            </tr>
        <?php endforeach; ?>
    </table>
    <br><br>
    <h3 class="text-right">سفارشات ارسال شده</h3>
    <br>
    <table class="table text-justify text-center table-hover" dir="rtl">
        <tr>
            <th>#</th>
            <th>نام و نام خانوادگی</th>
            <!--            <th>قیمت کل</th>-->
            <th>تعداد</th>
            <th>نام نمد</th>
            <th>آدرس</th>
            <th>تاریخ سفارش</th>
            <th>تاریخ تایید</th>
        </tr>

        <?php foreach (getSendedOrders() as $key => $value): ?>
            <?php
            $d = new DateTime($value['ordered_ts']);
            $u = new DateTime($value['sent_ts']);
            $jdate = new jDateTimePlus(true,true);
            ?>
            <tr>
                <?php $user = getUser($value['user_id']); ?>
                <td><?php echo $value['id'] ?></td>
                <td><?php echo $user['fname']." ".$user['lname'] ?></td>
                <!--                <td></td>-->
                <td><?php echo $value['quantity'] ?></td>
                <td><?php echo getProduct($value['product_id'])['title'] ?></td>
                <td class="text-justify" ><?php echo $value['address'] ?></td>
                <td><?php echo $jdate->date("l j F Y H:i",$d->getTimestamp()); ?></td>
                <td><?php echo $jdate->date("l j F Y H:i",$u->getTimestamp()); ?></td>
            </tr>
        <?php endforeach; ?>
    </table>

</div>