<div class="col-sm-10 chicoTextBlue pt-5 h-100" id="rightCol">
    <div class="row justify-content-center" v-if="!showNewNamad">
        <?php include_once TMP_BACK . DS . "adminCards.php" ?>
    </div>
    <div class="row p-5" v-if="showNewNamad" dir="rtl">
        <form class="p-4" action="http://localhost:81/chicofelt/resources/addProduct.php" enctype="multipart/form-data" method="post">
            <h3 class="text-right mb-5 chicoTextBlue">اطلاعات نمد را وارد کنید</h3>
            <div class="form-inline mb-4">
                <div class="form-group text-right ml-4">
                    <label for="title"  class="ml-3">نام نمد</label>
                    <input type="text" name="title" required id="title" class="form-control">
                </div>
                <div class="form-group">
                    <label for="file" class="ml-3">عکس محصول را انتخاب نمایید</label>
                    <input type="file" id="file" required name="location_img">
                </div>
            </div>
            <div class="form-inline">
                <div class="form-group ml-5">
                    <label for="price" class="ml-3">قیمت</label>
                    <input type="text" id="price" name="price" class="form-control" required>
                </div>
                <div class="form-group text-right ml-4">
                    <label for="desc" class="ml-3">توضیحات</label>
                    <textarea name="description" id="desc" required class="form-control" style="  width: 18rem;min-width:18rem;max-width:18rem;height:10rem;min-height:10rem;max-height:10rem;">
                </textarea>
                </div>
            </div>
            <br><br>
            <div class="w-75">
            <button class="btn btn-success btn-block " id="addProductBtn" type="submit" name="submit">ثبت</button>
            </div>

        </form>
    </div>
</div>