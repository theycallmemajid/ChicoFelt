<div class="col-sm-10 chicoTextBlue pt-5 h-100" id="products">

    <table class="table text-justify text-center table-hover table-responsive" dir="rtl">
        <tr>
            <th>#</th>
            <th>نام</th>
            <th>تصویر</th>
            <th>توضیحات</th>
            <th>تاریخ</th>
            <th>حذف</th>
            <th>ویرایش</th>
        </tr>

        <?php foreach (getAllProducts() as $key => $value): ?>
            <?php
            $d = new DateTime($value['created_ts']);
            $date = jdate("j F o  G:i", $d->getTimestamp());
            ?>
            <tr id="n<?php echo $value['id'] ?>">
                <td><?php echo $value['id'] ?></td>
                <td><?php echo $value['title'] ?></td>
                <td><img src="../../resources/uploads/<?php echo $value['location_img'] ?>" width="100px" height="45px"
                         alt=""></td>
                <td class="text-justify" ><?php echo truncate($value['description'],15)?></td>
                <td><?php echo $date ?></td>
                <td><a href="?deleteitem=<?php echo $value['id'] ?>"><span class="fa fa-times-circle text-danger"></span></a></td>
                <td><span class="fa fa-edit text-info editproduct" data-productid="<?php echo $value['id'] ?>"
                          style="cursor: pointer"></span></td>
            </tr>
        <?php endforeach; ?>
    </table>

    <div id="editProduct" style="display: none">
        <br><br>
        <h3 class="text-center">ویرایش اطلاعات نمد</h3>
        <hr>
        <br>
        <form method="post" class="text-right form-row" enctype="multipart/form-data">
            <div class="form-group col-sm-6">
                <label>
                    نام
                    <input type="text" name="title" id="title" class="form-control">
                </label>
            </div>

            <div class="form-group">
                <label>
                    قیمت
                    <input type="text" class="form-control" name="price" id="price">
                </label>
            </div>


            <div class="form-group col-sm-6">
                <label for="">
                    تصویر محصول
                    <input type="file" name="img" id="img" class="form-control">
                </label>
            </div>
            <div class="form-group ">
                <label>
                    توضیحات
                    <textarea class="form-control" style="font-size: 1rem;width: 500px;height: 250px;direction: rtl"
                              name="description" id="description"></textarea>
                </label>
            </div>
            <input type="hidden" id="hidden" name="id">
            <button type="submit" name="submit" class="btn btn-block btn-success">ثبت ویرایش</button>
        </form>
        <br>
        <br>
        <br>
        <br>
    </div>

</div>
<?php

if (isset($_POST['submit'])) {
    if (file_exists($_FILES['img']['tmp_name'])) {

        updateProductWithFile($_POST['id'], $_POST['title'], $_POST['price'], $_FILES['img'], $_POST['description']);
        redirectTo(ADMIN_URL . "productsManagement.php");

    } else {
        updateProduct($_POST['id'], $_POST['title'], $_POST['price'], $_POST['description']);
        redirectTo(ADMIN_URL . "productsManagement.php");

    }
}
if(isset($_GET['deleteitem'])){
    deleteProduct($_GET['deleteitem']);
    redirectTo(ADMIN_URL."productsManagement.php");
}
?>