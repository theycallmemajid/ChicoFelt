<div class="col-sm-2 chicoBlue text-white h-100 text-justify text-center pt-5" id="leftCol">
    <a href="<?php  echo ADMIN_URL."productsManagement.php"?>" class="d-block text-white w-100 p-3" style="border-bottom: 1px dashed white;text-decoration: none">
        <span>مدیریت نمدها</span>
        <span class="fa fa-tree"></span>
    </a>
    <a href="<?php  echo ADMIN_URL."usersManagement.php"?>" class="d-block text-white w-100 p-3" style="border-bottom: 1px dashed white;text-decoration: none">
        <span>مدیریت کاربران</span>
        <span class="fa fa-users"></span>
    </a>
    <a href="<?php  echo ADMIN_URL."ordersManagement.php"?>" class="d-block text-white w-100 p-3" style="border-bottom: 1px dashed white;text-decoration: none">
        <span>مدیریت خرید ها</span>
        <span class="fa fa-truck"></span>
    </a>
    <a href="<?php  echo ADMIN_URL."financesManagement.php"?>" class="d-block text-white w-100 p-3" style="border-bottom: 1px dashed white;text-decoration: none">
        <span>حسابداری</span>
        <span class="fa fa-calculator"></span>
    </a>



</div>