<div class="col-sm-10 chicoTextBlue pt-5 h-100" id="users">

    <table class="table text-justify text-center table-hover" dir="rtl">
        <tr>
            <th>#</th>
            <th>نام</th>
            <th>نام خانوادگی</th>
            <th>ایمیل</th>
            <th>موبایل</th>
            <th>گروه کاربری</th>
            <th>ویرایش</th>
<!--            <th>حذف</th>-->
        </tr>

        <?php foreach (getUsers() as $key => $value): ?>
            <tr id="<?php echo $value['id'] ?>" class="<?php if($value['role']=="admin"){echo "chicoBlue text-warning";}; ?>">
                <td><?php echo $value['id'] ?></td>
                <td><?php echo $value['fname'] ?></td>
                <td><?php echo $value['lname'] ?></td>
                <td><?php echo $value['email'] ?></td>
                <td><?php echo $value['mobile'] ?></td>
                <td><?php echo ($value['role']=="admin")?"مدیریت":"کاربر"?></td>
                <td><a href="" @click.prevent ><span  class="fa fa-pencil <?php echo ($value['role']=="admin")?"text-white":"text-dark"?> editButton" data-userid="<?php echo $value['id'] ?>" style="cursor: pointer"></span></a></td>
<!--                <td><span class="fa fa-times-circle text-danger xButton"  data-userid="--><?php //echo $value['id'] ?><!--" style="cursor: pointer" data-toggle="modal" data-target="#mymodal"></span></td>-->
            </tr>
        <?php endforeach; ?>

    </table>
    <div class="alert alert-info text-center" style="display: none" id="myalert">
        <span dir="rtl">کاربر با موفقیت حذف شد!</span>
        <span class="fa fa-info-circle"></span>
    </div>
    <br>
    <form action="" method="post"  class="text-center w-50 mx-auto" dir="rtl" style="font-size: .8rem; display: none;" id="editForm">
        <h4 class="chicoTextBlue">تغییر مشخصات کاربر</h4>
        <hr>
        <br>
        <div class="form-inline justify-content-around">
        <div class="form-group ">
            <input type="text" name="fname" id="fname" placeholder="نام" style="font-size: .8rem" class="form-control">
        </div>
        <div class="form-group ">
            <input type="text" name="lname" id="lname" placeholder="نام خانوادگی" style="font-size: .8rem" class="form-control">
        </div>
        </div>
        <br>
        <div class="form-group ">
            <input type="email" name="email" id="email" placeholder="ایمیل" style="font-size: .8rem" class="form-control">
        </div>
        <div class="form-group ">
            <input type="mobile" name="mobile" id="mobile" placeholder="موبایل" style="font-size: .8rem" class="form-control">
        </div>
        <select class="custom-select w-100" name="role">
            <option selected value="user">گروه کاربری</option>
            <option value="admin">ادمین</option>
            <option value="user">کاربر عادی</option>
        </select>
        <input type="hidden" name="id" value="" id="hidden">
        <br>
        <br>
        <button type="submit" name="submit" id="submit" class="btn btn-block btn-primary">ذخیره تغییرات</button>
    </form>
    <!-- Modal -->
    <div class="modal fade  mt-5" id="mymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">حذف کاربر</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-right">
                   آیا مایل به حذف این کاربر هستید؟
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                    <form method="post">
                    <button type="submit" name="submit" class="btn btn-danger" id="deleteUser" data-dismiss="modal">حذف کاربر</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if (isset($_POST['submit'])){
    updateUser($_POST['id'],$_POST['fname'],$_POST['lname'],$_POST['email'],$_POST['mobile'],$_POST['role']);
    redirectTo(ADMIN_URL);
}
?>
