<?php
require_once "config.php";
require_once 'jdf.php';
require_once "jdatetimeplus.class.php";


/***************Helper Functions****************/

function redirectTo($location)
{
    header("Location:$location");
}


function truncate($text, $limit)
{
    $strArr = explode(" ", $text);
    $str = "";
    if (count($strArr) > $limit) {
        for ($i = 0; $i < $limit; $i++) {
            $str .= $strArr[$i];
            $str .= " ";
        }
        return $str . "...";
    } else {
        return $text;
    }

// it takes 2 args that first is for passed text and second is number of words that it wants to truncate!
}


/***************Back-end Admin****************/


/***************GetProduct****************/


function getProducts($page)
{
    $item = ($page + (($page - 1) * (MAXITEM - 1))) - 1;
    global $conn;
    $sth = $conn->prepare("SELECT * FROM " . productsTable . " ORDER BY created_ts DESC LIMIT $item ," . MAXITEM);
    $sth->execute();

    return $result = $sth->fetchAll(PDO::FETCH_ASSOC);


//    returns an associative array of that table;
}

function getAllProducts()
{
    global $conn;
//    global $productsTable;
    $sth = $conn->prepare("SELECT * FROM " . productsTable . " ORDER BY created_ts DESC");
    $sth->execute();

    return $result = $sth->fetchAll(PDO::FETCH_ASSOC);


//    returns an associative array of that table;
}

function getOrders()
{
    global $conn;
//    global $productsTable;
    $sth = $conn->prepare("SELECT * FROM " . ordersTable . " WHERE order_status='submited' ORDER BY ordered_ts DESC");
    $sth->execute();

    return $result = $sth->fetchAll(PDO::FETCH_ASSOC);


//    returns an associative array of that table;
}

function getUsers()
{
    global $conn;
//    global $productsTable;
    $sth = $conn->prepare("SELECT * FROM " . usersTable . " ORDER BY lname ASC");
    $sth->execute();

    return $result = $sth->fetchAll(PDO::FETCH_ASSOC);


//    returns an associative array of that table;
}

function productsCount()
{
    global $conn;
//    global $productsTable;
    $sth = $conn->prepare("SELECT COUNT(id) AS cnt FROM " . productsTable);
    $sth->execute();

    return $result = $sth->fetch(PDO::FETCH_ASSOC);
}

function usersCount()
{
    global $conn;
//    global $productsTable;
    $sth = $conn->prepare("SELECT COUNT(id) AS cnt FROM " . usersTable);
    $sth->execute();

    return $result = $sth->fetch(PDO::FETCH_ASSOC);
}

function ordersCount()
{
    global $conn;
//    global $productsTable;
    $sth = $conn->prepare("SELECT COUNT(id) AS cnt FROM " . ordersTable);
    $sth->execute();

    return $result = $sth->fetch(PDO::FETCH_ASSOC);
}

function getProduct($id)
{
    global $conn;

    $sth = $conn->prepare("SELECT * FROM " . productsTable . " WHERE id LIKE :id");
    $sth->bindParam(':id', $id);
    $sth->execute();

    return $result = $sth->fetch(PDO::FETCH_ASSOC);


//    returns an associative array of that Product;
}

function getUser($id)
{
    global $conn;

    $sth = $conn->prepare("SELECT * FROM " . usersTable . " WHERE id LIKE :id");
    $sth->bindParam(':id', $id);
    $sth->execute();

    return $result = $sth->fetch(PDO::FETCH_ASSOC);


//    returns an associative array of that Product;
}

function getRate($product)
{
    global $conn;
    $sth = $conn->prepare("SELECT COUNT(id) AS cnt,AVG(rate_number) AS ave FROM " . ratesTable . " WHERE product_id LIKE :product");
    $sth->bindParam(':product', $product);
    $sth->execute();
    return $result = $sth->fetch(PDO::FETCH_ASSOC);

}
function getOrderedNumber($number){
    for($i=strlen($number);$i==0;$i--){

    }
}

function getOrdersSumSale()
{
    global $conn;
    $sth = $conn->prepare("SELECT SUM((SELECT price FROM products WHERE products.id = orders.product_id)*quantity) AS jam  FROM " . ordersTable . "");
    $sth->execute();
    return $result = $sth->fetch(PDO::FETCH_ASSOC);

}

function getOrdersNumberOfSale()
{
    global $conn;
    $sth = $conn->prepare("SELECT COUNT(orders.id) AS cnt  FROM " . ordersTable . " WHERE  order_status='sended'");
    $sth->execute();
    return $result = $sth->fetch(PDO::FETCH_ASSOC);

}


/***************Auth****************/

function loggIn($email, $pass)
{
    global $conn;
    $pass=getHash($pass);
    $sql = "SELECT * FROM " . usersTable . " WHERE email LIKE :email AND pass LIKE :password ";
    try {
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':password', $pass);
        $stmt->execute();
        $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($res) === 0) {
            return false;
        } else {
            $_SESSION['logined'] = true;
            $_SESSION['user_id'] = $res[0]['id'];
            $user=$_SESSION['user_id'];
            logMessage("User with id($user) logged in successfully!");
            if ($res[0]['role'] == 'admin') {
                $_SESSION['isAdmin'] = true;

            }

            $_SESSION['showingName'] = $res[0]['fname'] . " " . $res[0]['lname'];

        }
        redirectTo(HOME_URL);
        return true;
    } catch (PDOException $p) {
        die("Something Went Wrong!");
    }


}

function isLoggedIn()
{
    if (isset($_SESSION['logined'])) {
        return true;
    } else {
        return false;
    }
}

function isAdmin()
{
    if (isset($_SESSION['isAdmin'])) {
        return true;
    } else {
        return false;
    }
}

function getHash($msg)
{
    $salt = "chicochico";
    return md5($salt . $msg . $salt);
}
function logMessage($msg){
    $file = __DIR__."/log-".date("Y")."-".date("m").".txt";
    file_put_contents($file,date("Y-m-d H:i:s")."-> ".$msg.PHP_EOL,FILE_APPEND);
}


function logOut()
{
    unset($_SESSION['logined']);
    if (isAdmin()) {
        unset($_SESSION['isAdmin']);
    }
    $user=$_SESSION['user_id'];
    unset($_SESSION['user_id']);
    unset($_SESSION['showingName']);
    unset($_SESSION['products']);
    logMessage("user with id($user) logged out.");
    redirectTo(HOME_URL);
}

function addUser($fname, $lname, $email, $mobile, $passOne, $passTwo)
{
    global $conn;
    $pass = getHash($passOne);
    $reg = '/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';
    if ($passOne !== $passTwo) {
        return false;
    }
    if (!preg_match("/09([123])\d{8}/", $mobile)) {
        return false;
    }
    if (!preg_match($reg, $email)) {
        return false;
    }
    $sql = "INSERT INTO " . usersTable . "(id,fname,lname,email,mobile,pass,role) VALUES (NULL,:fname,:lname,:email,:mobile,:password,'user')";
    try {
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':fname', $fname);
        $stmt->bindParam(':lname', $lname);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':mobile', $mobile);
        $stmt->bindParam(':password', $pass);
        $res = $stmt->execute();
        if ($res) {
            $_SESSION['logined'] = true;
            $_SESSION['showingName'] = $fname . ' ' . $lname;
            redirectTo(HOME_URL);
            return true;
        } else {
            return false;
        }


    } catch (PDOException $p) {
        var_dump($p->errorInfo);
        die("Something Went Wrong!");
    }

}

function deleteUser($ide)
{
    global $conn;
    $sth = $conn->prepare("DELETE FROM " . usersTable . " WHERE id = :id");
    $sth->bindParam(':id', $ide);
    $sth->execute();


}

function getSendedOrders()
{
    global $conn;
    $sth = $conn->prepare("SELECT * FROM " . ordersTable . " WHERE order_status='sended' ORDER BY ordered_ts DESC");
    $sth->execute();

    return $result = $sth->fetchAll(PDO::FETCH_ASSOC);


//    returns an associative array of that table;
}

function updateUser($id, $fname, $lname, $email, $mobile, $role)
{
    global $conn;
    $sth = $conn->prepare("UPDATE " . usersTable . " SET fname=:fname,lname=:lname,email=:email,mobile=:mobile,role=:role WHERE id=:id");
    $sth->bindParam(':id', $id);
    $sth->bindParam(':fname', $fname);
    $sth->bindParam(':lname', $lname);
    $sth->bindParam(':email', $email);
    $sth->bindParam(':mobile', $mobile);
    $sth->bindParam(':role', $role);

    return $sth->execute();

    logMessage("user with id($id) info updated.");


//    returns an associative array of that table;
}
function currentUser(){
    if(isLoggedIn()){
        return $_SESSION['user_id'];
    }
}
function addOrder($user,$pid,$quantity,$address){
    global $conn;
    $sth = $conn->prepare("INSERT INTO  " . ordersTable . "(id,user_id,product_id,quantity,address) VALUES (NULL,:userid,:product,:quantity,:address) ");
    $sth->bindParam(':userid', $user);
    $sth->bindParam(':product', $pid);
    $sth->bindParam(':quantity', $quantity);
    $sth->bindParam(':address', $address);
    return $sth->execute();

    logMessage("user with id($id) bought product with id($pid) .");
}
function addRate($user,$product,$ratenum){
    global $conn;
    $sth = $conn->prepare("INSERT INTO  " . ratesTable . "(user_id,product_id,rate_number) VALUES (:userid,:product,:ratenum) ");
    $sth->bindParam(':userid', $user);
    $sth->bindParam(':product', $product);
    $sth->bindParam(':ratenum', $ratenum);
    return $sth->execute();
    logMessage("user with id($user) rated product with id($product) .");
}
function isRated($user,$product){
    global $conn;
    $sth = $conn->prepare("SELECT COUNT(id) AS cnt FROM " . ratesTable . " WHERE user_id = :userid AND product_id=:product");
    $sth->bindParam(':userid', $user);
    $sth->bindParam(':product', $product);
    $sth->execute();
    if (($sth->fetch(PDO::FETCH_ASSOC)['cnt']) > 0) {
        return true;
    } else {
        return false;
    }
}

function sendOrder($order)
{
    global $conn;
    $sth = $conn->prepare("UPDATE " . ordersTable . " SET order_status='sended' WHERE id=:orderId");
    $sth->bindParam(':orderId', $order);
    return $sth->execute();
    logMessage("order with id($order) sent out.");
}

function updateProduct($id, $title, $price, $description)
{
    global $conn;
    $stmt = $conn->prepare("UPDATE " . productsTable . " sET title=:title,description=:description,price=:price WHERE id=:id");
    $stmt->bindParam(":title", $title);
    $stmt->bindParam(":id", $id);
    $stmt->bindParam(":description", $description);
    $stmt->bindParam(":price", $price);
    $stmt->execute();

    logMessage("product with id($id) updated without file.");


}

;
function updateProductWithFile($id, $title, $price, $img, $description)
{
    if (move_uploaded_file($img['tmp_name'], UPLOADS . $img['name'])) {
        unlink(UPLOADS . getProduct($id)['location_img']);
        global $conn;
        $stmt = $conn->prepare("UPDATE " . productsTable . " sET title=:title,location_img=:img,description=:description,price=:price WHERE id=:id");
        $stmt->bindParam(":title", $title);
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":img", $img['name']);
        $stmt->bindParam(":description", $description);
        $stmt->bindParam(":price", $price);
        $stmt->execute();
        logMessage("product with id($id) updated with file.");
        return true;
    } else {
        return false;
    }
}

;
function deleteProduct($ide)
{
    global $conn;
    $productImage = getProduct($ide)['location_img'];
    $stmt = $conn->prepare("DELETE FROM " . productsTable . " WHERE id = :id");
    $stmt->bindParam(":id", $ide);
    $stmt->execute();
    unlink(UPLOADS .$productImage);
    logMessage("product with id($ide) deleted.");

}

function ratedBefore($user, $product)
{
    global $conn;
    $stmt = $conn->prepare("SELECT COUNT(id) AS cnt FROM " . ratesTable . " WHERE user_id = :userid AND product_id = :productid ");
    $stmt->bindParam(":userid", $user);
    $stmt->bindParam(":productid", $product);
    $stmt->execute();
    if ($stmt->fetch(PDO::FETCH_ASSOC)['cnt'] > 0) {
        return true;
    } else {
        return false;
    }

}

function isOrdered($user, $product)
{
    global $conn;
    $stmt = $conn->prepare("SELECT COUNT(id) AS cnt FROM " . ordersTable . " WHERE user_id=:userid AND product_id=:productid");
    $stmt->bindParam(":userid", $user);
    $stmt->bindParam(":productid", $product);
    $stmt->execute();
    if (($stmt->fetch(PDO::FETCH_ASSOC)['cnt']) > 0) {
        return true;
    } else {
        return false;
    }
}

function rateNum($user, $product, $ratenum)
{
    global $conn;
    $rate = $conn->prepare("INSERT INTO " . ratesTable . " (id,product_id,user_id,rate_number) VALUES (NULL,:productid,:userid,:ratenum) ");
    $rate->bindParam(":userid", $user);
    $rate->bindParam(":productid", $product);
    $rate->bindParam(":ratenum", $ratenum);
    $rate->execute();
}



