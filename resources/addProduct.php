<?php
header("content-type: application/json");
require_once 'functions.php';
if (isset($_POST['submit'])) {
    $title = $_POST['title'];
    $desc = $_POST['description'];
    $price = $_POST['price'];
    $image = $_FILES['location_img']['name'];
    $imgtmp = $_FILES['location_img']['tmp_name'];
    if(move_uploaded_file($imgtmp,UPLOADS.$image)){
        global $conn;
        $stmt = $conn->prepare("INSERT INTO " . productsTable . " (id,title,location_img,description,price) VALUES (null,:title,:img,:description,:price)");
        $stmt->bindParam(":title", $title);
        $stmt->bindParam(":img", $image);
        $stmt->bindParam(":description", $desc);
        $stmt->bindParam(":price", $price);
        $stmt->execute();
        redirectTo(ADMIN_URL);
    }else{
    redirectTo(NOTFOUND_URL);
    }

}
