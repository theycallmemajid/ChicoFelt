<?php
header("content-type: application/json");
require_once 'functions.php';
if (isset($_POST['search'])) {
    $s = $_POST['search'];
    global $conn;
    $s = explode(" ", $s);
    $s = implode("%", $s);
    $s = "%" . $s . "%";
    $stmt = $conn->prepare("SELECT id,title,created_ts,description FROM " . productsTable . " WHERE title LIKE :search OR description LIKE :search ORDER BY created_ts DESC LIMIT 4  ");
    $stmt->bindParam(":search", $s);
    $stmt->execute();
    $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
//    truncating description for display better in search form
    for ($i = 0; $i < count($res); $i++) {
        $res[$i]['description'] = truncate($res[$i]['description'], 20);
    }
    echo json_encode($res);
}
