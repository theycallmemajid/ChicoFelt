<?php
header("content-type: application/json");
require_once 'functions.php';
if (isset($_GET['product_id'])){
    echo json_encode(getProduct($_GET['product_id']));
}