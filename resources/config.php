<?php
ob_start();
session_start();
ini_set("error_reporting","on");
error_reporting(E_ALL);
/*-----------Path and URL Configs-----------*/

defined("DS") ? null : define("DS", DIRECTORY_SEPARATOR);
defined("TMP_BACK") ? null : define("TMP_BACK", __DIR__ . DS . "templates/back");
defined("TMP_FRONT") ? null : define("TMP_FRONT", __DIR__ . DS . "templates/front");
defined("UPLOADS") ? null : define("UPLOADS", __DIR__ . DS . "uploads".DS);
defined("HOME_URL") ? null : define("HOME_URL", "http://localhost:81/chicofelt/public/");
defined("CART_URL") ? null : define("CART_URL", "http://localhost:81/chicofelt/public/cart.php");
defined("ADMIN_URL") ? null : define("ADMIN_URL", "http://localhost:81/chicofelt/public/admin/");

//error_reporting(E_ALL);
error_reporting(0);
/*-----------Numbers-----------*/


defined("MAXITEM") ? null : define("MAXITEM","6");


/*-----------DB Configs-----------*/


defined("DBHOST") ? null : define("DBHOST", "localhost");
defined("DBUSER") ? null : define("DBUSER", "root");
defined("DBPASS") ? null : define("DBPASS", "");
defined("DBNAME") ? null : define("DBNAME", "chicofelt");



/*-----------DB Vars-----------*/


defined("usersTable") ? null : define("usersTable", "users");
defined("productsTable") ? null : define("productsTable", "products");
defined("ordersTable") ? null : define("ordersTable", "orders");
defined("resetPassTable") ? null : define("resetPassTable", "reset_pass");
defined("ratesTable") ? null : define("ratesTable", "rates");

/*-----------Connections-----------*/

try {
    $conn = new PDO("mysql:host=" . DBHOST . ";dbname=" . DBNAME, DBUSER, DBPASS);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sth = $conn->prepare("SET NAMES 'utf8'");
    $sth ->execute();





} catch (PDOException $e) {
    die("Oops " . "<br>" . $e->getMessage());
}
